extends CharacterBody3D

signal double_jump
signal first_hop
signal second_hop
signal third_hop

var firstHop : bool = true
var secondHop : bool = true
var thirdHop : bool = true

var was_on_floor : bool = false


var coyoteTimer = Timer.new()
var firstHopTimer = Timer.new()
var secondHopTimer = Timer.new()

const SPEED = 5.0
const JUMP_VELOCITY = 4.5

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")


func _ready():
	pass
	# add_child(coyoteTimer)
	# coyoteTimer.wait_time = 0.0017 * 6
	# coyoteTimer.one_shot = true
	# coyoteTimer.timeout.connect("reset_hop")

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta

	# Handle jump.
	#if Input.is_action_just_pressed("ui_accept") and is_on_floor():
		#velocity.y = JUMP_VELOCITY

	if Input.is_action_just_pressed("action_a") and is_on_floor():
		if firstHop == true:
			print("first hop")
			velocity.y = JUMP_VELOCITY
			firstHop = false
			add_child(firstHopTimer)
			firstHopTimer.wait_time = 5
			firstHopTimer.one_shot = true
			firstHopTimer.start()
			firstHopTimer.timeout.connect(reset_hops)
		elif secondHop == true:
			print("second hop")
			velocity.y = JUMP_VELOCITY * 1.5
			secondHop = false
		elif thirdHop == true:
			print("third hop")
			velocity.y = JUMP_VELOCITY * 2
			thirdHop = false
			reset_hops()

	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var input_dir = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * SPEED
		velocity.z = direction.z * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.z = move_toward(velocity.z, 0, SPEED)

	move_and_slide()

func is_coyote_timer_running():
	if not coyoteTimer.is_stopped():
		return true
	return false

func is_first_hop_timer_running():
	if not firstHopTimer.is_stopped():
		return true
	return false

func is_second_hop_timer_running():
	if not secondHopTimer.is_stopped():
		return true
	return false

func reset_hops():
	firstHop = true
	secondHop = true
	thirdHop = true


func reset_first_hop():
	firstHop = true

func reset_second_hop():
	if is_first_hop_timer_running():
		firstHopTimer.stop()
		firstHopTimer.queue_free()
	secondHop = true
