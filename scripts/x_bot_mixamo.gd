extends CharacterBody3D

# Chain Tripple Jump
signal first_jump
signal second_jump
signal third_jump

# Double Jump
signal double_jump

# Landed
signal jump_consumed
signal reset_jumpState
signal just_landed

const SPEED = 5.0
const MAX_SPEED = 20.0
const ACCELERATION = 1.5
const FIRST_JUMP_VELOCITY = 4.5
const SECOND_JUMP_VELOCITY = 7
const THIRD_JUMP_VELOCITY = 10

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

var doubleJump : bool = true
var firstJump : bool = true
var secondJump : bool = true
var thirdJump : bool = true

var was_on_floor : bool = false

#https://youtu.be/UpF7wm0186Q?t=179
#var just_landed := is_on_floor() and _snap_vector == Vector3.ZERO

#https://www.reddit.com/r/godot/comments/15v7a1e/how_to_detect_landinggodot_35_2d/?utm_source=share&utm_medium=web2x&context=3
#var was_in_air: bool = not is_on_floor()
#var just_landed: bool = is_on_floor() and was_in_air


func _input(event):
	pass

func _physics_process(delta):
	# Add the gravity.
	var was_in_air: bool = not is_on_floor()

	if is_on_floor():
		doubleJump = true

	if not is_on_floor():
		velocity.y -= gravity * delta

	if is_on_floor() and not was_on_floor:
		print(was_on_floor)
		was_on_floor = true
		emit_signal("just_landed",delta)

	# Handle jump.
	#if Input.is_action_just_pressed("ui_accept") and is_on_floor():
		#velocity.y = FIRST_JUMP_VELOCITY

	if Input.is_action_just_pressed("action_a") and is_on_floor() and firstJump == true:
		velocity.y = FIRST_JUMP_VELOCITY
		was_on_floor = false
	elif Input.is_action_just_pressed("action_a") and is_on_floor() and firstJump == false and  secondJump == true:
		velocity.y = SECOND_JUMP_VELOCITY
		was_on_floor = false
		emit_signal("second_jump")
	elif Input.is_action_just_pressed("action_a") and is_on_floor() and firstJump == false and  secondJump == false and thirdJump == true :
		velocity.y = THIRD_JUMP_VELOCITY
		was_on_floor = false
		emit_signal("third_jump")
		#emit_signal("third_jump")
		#firstJump = false
		#secondJump = false
		#thirdJump = false
	elif Input.is_action_just_pressed("action_a") and not is_on_floor():
		emit_signal("double_jump")


	# Get the input direction and handle the movement/deceleration.
	# As good practice, you should replace UI actions with custom gameplay actions.
	var input_dir = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * SPEED
		velocity.z = direction.z * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.z = move_toward(velocity.z, 0, SPEED)

	# Long Jump ????
	if direction and Input.is_action_just_pressed("action_a") and Input.is_action_just_pressed("action_z"):
		#velocity.x = direction.x * SPEED
		#velocity.z = direction.z * SPEED
		print("ayo")
	move_and_slide()

func _ready():
	self.double_jump.connect(_on_double_jump)
	self.jump_consumed.connect(_double_jump_state)
	
	self.just_landed.connect(_on_landed_timer)
	

func _process(delta):
	pass


func _on_Jump_Timer_timeout(jump_name : String = "first_jump"):
	print(jump_name)
	firstJump = true

func _on_Timer_timeout():
	print("ayoayo")
	firstJump = true
#timerFirstJump
#_on_Timer_firstJump():
func _on_Timer_firstJump():
	firstJump = true 
func _on_landed_timer(delta):
	print("back sur le plancher des vache")
	var chain_jump_timer = Timer.new()
	add_child(chain_jump_timer)
	chain_jump_timer.wait_time = 5
	chain_jump_timer.one_shot = true
	chain_jump_timer.start()
	if Input.is_action_just_pressed("action_a") and chain_jump_timer.time_left > 0:
		chain_jump_timer.stop()
		print("j'ai jump attends")
	#if Input.is_action_just_pressed("action_a")
	

func _on_double_jump():
	if doubleJump == true :
		print("double")
		velocity.y += 5
		emit_signal("jump_consumed")
	else :
		print("Nope you can't")

func _double_jump_state():
	doubleJump = false

func _first_jump_done(delta):
	firstJump = false
	var timerFirstJump = Timer.new()
	add_child(timerFirstJump)
	timerFirstJump.wait_time = delta
	timerFirstJump.one_shot = true
	timerFirstJump.start()
	timerFirstJump.timeout.connect(_on_Jump_Timer_timeout)

func _second_jump_done():
	secondJump = false

func _third_jump_done():
	thirdJump = false

func _reset_jumps_chain():
	firstJump = true
	secondJump = true
	thirdJump = true

func _break_jump_chain():
	print("break Jump Chain")
	
func _testo():
	print("testo")
